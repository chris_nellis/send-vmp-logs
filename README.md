## This repo serves as part of my professional portfolio @ chrisnellis.com

> This project was built to facilitate the collection of server logs for one of the applications I was supporting at the time. The application could gather data on the server specs and configuration, as well as the server logs themselves, and then zip them up to optionally be sent via FTP.