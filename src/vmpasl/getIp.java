/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vmpasl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cnellis
 */
public class getIp {

    static String ipString;
    
public static String getCurrentIpString() {
   
        try {
            InetAddress ipAdd =InetAddress.getLocalHost();
            ipString = ipAdd.getHostAddress();
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(getIp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ipString;
    }
}

