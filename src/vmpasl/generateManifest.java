/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vmpasl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author cnellis
 */
public class generateManifest {
    
    public static String makeManifest(String custName, String custID, String custLicKey, String aslTempPath) {
        
        // Starting to create the manifest file
        String cName = custName;
        String cId = custID;
        String cLicKey = custLicKey;
        String serverType = "VMP";
        
        String tempPath = aslTempPath;
        String dateStamp = generateDateTime.getManifestStamp();
        String serverIp = getIp.getCurrentIpString();
        String br = "\r\n";
        String tab = "\t";
        String mFileName = dateStamp + ".manifest";
        String mFilePath = tempPath + "\\" + mFileName;
        
        File tempFolder = new File(tempPath);
        File[] listOfFiles = tempFolder.listFiles();        
        
        BufferedWriter manifestOut;
            try {
                manifestOut = new BufferedWriter(new FileWriter(mFilePath));
                manifestOut.write("[CustomerName]=" + cName + br);
                manifestOut.write("[CaseNumber]=None" +br); // Not implementing this feature yet
                manifestOut.write("[cID]=" + cId + br);
                manifestOut.write("[LKey]=" + cLicKey + br);
                manifestOut.write("[Type]=" + serverType + br);
                manifestOut.write("[IP]=" + serverIp + br);
                manifestOut.write("[Locale]=None" + br); // Locale information not available yet (maybe from Windows?)
                manifestOut.write("[ASLv]=1.00-vmp" + br); // Subject to change based on CAFI requirements
                manifestOut.write("[Files]=" + br);
                    for (int i = 0; i < listOfFiles.length; i++) {
                        manifestOut.write(tab + listOfFiles[i] + br);
                        }
                manifestOut.write(tab + mFileName + br);
                    
        // Cleaning up
        manifestOut.close();
        
        }catch(IOException e){
            System.out.println("There was a problem when generating system information:" + e);
            return "manifest could not be created";
    }
        return mFilePath;
    }
}
